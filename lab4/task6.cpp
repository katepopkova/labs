/*
�������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������:
����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������.
*/

#include<stdio.h>
#include<stdlib.h>
#include<locale.h>
int main()
{
    char name[30][150]; 
    int age[30];
    int *old_age, *young_age;
    char *old_name, *young_name;
    int i=0, n;
    setlocale(LC_ALL, "rus");
    printf("������� ���������� �������������: ");
    scanf("%d", &n);
    printf("������� ��� (�_�_�) ������������ � ��� �������:\n");
    while(i<n)
    {
        fflush(stdin);
        scanf("%s%d", &name[i], &age[i]);
        ++i;
    }
    young_age=age;
    old_age=age;
    for(i=0;i<n;i++)
    {
        if(age[i]<*young_age)
        {
            young_age=&age[i];
            young_name=&name[i][0];
        }
        else
        {
            old_age=&age[i];
            old_name=&name[i][0];
        }
    }
    printf("\n");
    printf("%s%s%c\n", "����� ������� ����������� - ��� ", young_name, '.');
    printf("%s%s%c\n", "����� ������ ����������� - ��� ", old_name, '.');
	printf("\n");
    system("pause");
    return 0;
}