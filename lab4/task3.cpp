/*
Ќаписать программу, которая запрашивает строку и определяет, не является
ли строка палиндромом (одинаково читаетс€ и слева направо и справа налево).
Замечание:
Цель задачи - применить указатели для быстрого сканирования строки с двух
концов.
*/

#include<stdio.h>
#include<locale.h>
#include<stdlib.h>
#include<string.h>
int main()
{
	char str[150];
	char *pstr[2];
	int i, j, len, k=0;
	setlocale(LC_ALL, "rus");
	printf("Введите строку: ");
	gets(str);
	len=strlen(str);
	pstr[0]=str;
	pstr[1]=str+len-1;
	for(i=0,j=len-1; i<=j; i++,j--)
	{
		if(*(pstr[0]+i)!=*(pstr[1]-(len-1-j)))
		{
			printf("Строка не является полиндромом.\n");
			break;
		}
		else 
		{
			++k;
		}
	}
	if(k==i)
		printf("Строка является полиндромом.\n");
	system("pause");
	return 0;
}