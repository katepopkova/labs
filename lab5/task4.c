/*
�������� ���������, ������� ������ ��������� ��������� ���� �
������������ �������� ����� � ������ ������.
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ���������� �������, ������������� � ������ ������
1.
*/

#include <locale.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define M 200
#define N 150

char getWords(char *table, char **pWord, int len, int k);
void printWord(char *t);

int count=0;

int main()
{
	char table[M][N];
	char *pWord[M];
	char *t;
	int len, ind;
	int i, k=0;
	FILE *pfile;
	setlocale(LC_ALL, "rus");
	pfile=fopen("task45.txt", "r"); 
	if(pfile==NULL)
		perror("������ �������� �����.");
	else
	{
		srand(time(NULL));
		while (fgets(table[k], M, pfile))
		{
			if (table[k][0]=='\n')
				break;
			else
			{
				len=strlen(table[k]);
				getWords(table[k], pWord, len, k);
				pfile=fopen("task45.txt", "a+"); 
				for(i=0; i<count; i++)
				{
					ind=rand()%(i-count);
					printWord(pWord[ind]);
					t=pWord[ind];
					pWord[ind]=pWord[count-i-1];
					pWord[count-i-1]=t;
				}	
				printf("\n");
			}
			++k;
		}
	fclose(pfile);
	system("pause");
	return 0;
}

char getWords(char *table, char **pWord, int len, int k)
{
	int i;
	short fl=0;
	for(i=0;i<len;i++)
	{
		if(table[k][i]!=' ' && fl==0)
		{
			pWord[count]=&table[k][i];
			++count;
			fl=1;
		}
		else if(table[k][i]==' ' && fl==1)
		{
			table[k][i]='\0';
			fl=0;
		}
	}
	return **pWord;
}

void printWord(char *t)
{
	while (*t!='\0')
	{
		putchar(*t);
		if(*(t+1)!='\0')
			t++;
		else
			break;
	}
	putchar(' ');
}
