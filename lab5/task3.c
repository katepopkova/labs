/*
Написать программу, переставляющую случайным образом симво-
лы каждого слова каждой строки текстового файла, кроме первого
и последнего, то есть начало и конец слова меняться не должны.
Замечание:
Программа открывает существующий тектстовый файл и читает его построч-
но. Для каждой строки выполняется разбивка на слова и независимая обра-
ботка каждого слова.
*/

#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define M 250
#define N 200
#define L 110

void cutStr(char (*table)[N], char **pWord, int *count);
void swap(char **pWord);

int main()
{
	char table [M][N];
	char *ptable[M];
	char *pWord[L];
	int i, k=0;
	FILE *pfile;
	setlocale(LC_ALL, "rus");
	pfile=fopen("task35.txt", "r"); 
	if(pfile==NULL)
		perror("Ошибка открытия файла");
	else
	{
		while (fgets(table[k], M, pfile))
		{
			if (table[k][0]=='\n')
				break;
			else
			{
				ptable[k]=&table[k][0];
				k++;
			}
		}
		cutStr(table, pWord, &k);
		for(i=0; i<k; i++)
			swap(pWord);
		pfile=fopen("task35.txt", "a+");
		fprintf(pfile, "%s", "\n");
		for(i=0; i<k; i++) 
			fprintf(pfile, "%s", ptable[i]);
	}
	fclose(pfile);
	system("pause");
	return 0;
}

void cutStr(char (*table)[N], char **pWord, int *count) //разбивка строки на слова
{
	int i;
	int len;
	short fl=0;
	len=strlen(*(&table));
	for(i=0; i<len; i++)
	{
		if(table[i]!=' ' && fl==0)
		{
			pWord[*count]=&table[i];
			fl=1;
			++(*count);
		}
		else if(table[i]==' ' && fl==1)
		{
			table[i]='\0';
			fl=0;
		}
	}
}

void swap(char **pWord) //функция перестановки символов в слове
{
	int count, num;
	int i;
	char t;
	srand(time(NULL));  
	count=strlen(**(&pWord))-1;
	for(i=0; i<count-1; i++)
	{
		num=rand()%(count-i-1);
		t=pWord[i];
		pWord[i]=pWord[i+num];
		pWord[i+num]=t;
	}

}