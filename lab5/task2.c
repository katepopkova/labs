/*
Ќаписать программу "Калейдоскоп", выводящую на экран изобра-
жение, составленное из симметрично расположенных звездочек '*'.
Изображение формируется в двумерном символьном массиве, в од-
ной его части и симметрично копируется в остальные его части.
Замечание:
Решение задачи протекает в виде следующей последовательности шагов:
1) Очистка массива (заполнение пробелами)
2) Формирование случайным образом верхнего левого квадранта (занесение
'*')
3) Копирование символов в другие квадранты массива
4) Очистка экрана
5) Вывод массива на экран (построчно)
6) Временная задержка.
7) Переход к шагу 1.
*/

#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <dos.h>

#define SIZE 20
#define N 20

void kaleidoscope();
void clearArr(char (*arr)[SIZE]);
void randArr(char (*arr)[SIZE]);
void copyArr(char (*arr)[SIZE]);
void printArr(char (*arr)[SIZE]);

int main()
{
	int i;
	for(i=0; i<N; i++)
		kaleidoscope();
	system("pause");
	return 0;
}

void kaleidoscope()
{
	char arr[SIZE][SIZE];
	clearArr(arr);
	randArr(arr);
	copyArr(arr);
	system("clr");
	printArr(arr);
	Sleep(2000);
}

void clearArr(char (*arr)[SIZE]) //функция очистки массива
{
	int i, j;
	for(i=0; i<SIZE; i++)
		for(j=0; j<SIZE; j++)
			arr[i][j]=' ';
}

void randArr(char (*arr)[SIZE]) //функция формирования массива из '*' 
{
	char var[]={'*', ' '};
	int i, j;
	for(i=0; i<SIZE/2; i++)
		for(j=0; j<SIZE/2; j++)
			arr[i][j]=var[rand()%(sizeof var/sizeof (char))];
}

void copyArr(char (*arr)[SIZE]) //копирование символов в другие квадранты массива
{
	int i, j, k=1;
	for(i=0; i<SIZE/2; i++)
	{
		for(j=0; j<SIZE/2; j++) //заполнение нижнего левого квадранта
			arr[i+(SIZE-k)][j]=arr[i][j];
		k+=2;
	}
	for(i=0; i<SIZE; i++) //заполнение второй половины массива
	{
		k=1;
		for(j=0; j<SIZE/2; j++)
		{
			arr[i][j+(SIZE-k)]=arr[i][j];
			k+=2;
		}
	}
}

void printArr(char (*arr)[SIZE]) //печать массива
{
	int i, j;
	for(i=0; i<SIZE; i++)
	{
		for(j=0; j<SIZE; j++)
			printf("%c", arr[i][j]);
		printf("\n");
	}
}