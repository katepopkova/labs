/*
�������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
���������:
��������� ������ �������� ������� �� ���� �������:
a) printWord - ������� ����� �� ������ (�� ����� ������ ��� �������).
b) getWords - ��������� ������ ���������� �������� ������ ���� ����.
c) main - �������� �������.
*/

#include<locale.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

int count=0;

char getWords(char *str, char **pWord, int len);
void printWord(char *t);

int main()
{
	char str[200];
	char *pWord[110];
	char *t;
	int len, i, ind;
	setlocale(LC_ALL, "rus");
	srand(time(NULL));
	printf("������� ������: ");
	gets(str);
	len=strlen(str);
	getWords(str, pWord, len);
	for(i=0;i<count;i++)
	{
		ind=rand()%(i-count);
		printWord(pWord[ind]);
		t=pWord[ind];
		pWord[ind]=pWord[count-i-1];
		pWord[count-i-1]=t;
	}
	printf("\n");
	system("pause");
	return 0;
}

char getWords(char *str, char **pWord, int len)
{
	int i;
	short fl=0;
	for(i=0;i<len;i++)
	{
		if(str[i]!=' ' && fl==0)
		{
			pWord[count]=&str[i];
			++count;
			fl=1;
		}
		else if(str[i]==' ' && fl==1)
		{
			str[i]='\0';
			fl=0;
		}
	}
	return **pWord;
}

void printWord(char *t)
{
	while (*t!='\0')
	{
		putchar(*t);
		if(*(t+1)!='\0')
			t++;
		else
			break;
	}
	putchar(' ');
}