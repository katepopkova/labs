/*
�������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� �� ������. �
������ ������������� n ��������� ��������� �� ������.
���������:
� ���������� ������ ��������� ������� ������ ���� ������� �� �������.
��������� �������������� ������� ������.
*/

#include<stdio.h>
#include<locale.h>
#include<stdlib.h>
#include<string.h>
int main()
{
	char str[300];
    int i, j, k, n, sp, len, count=0, len2, cosp=0, col=0;
	setlocale(LC_ALL, "rus");
	printf("������� ������ �� ���� (��������� �� ��������): ");
	gets(str);
	len=strlen(str);
	for(i=0;i<len;i++) 
		if(str[i]!=' ')
			continue;
		else
			++cosp;
	while(1) //�������� ������������ n
	{
		printf("������� ����� �����: ");
		scanf("%d", &n);
		if((n>cosp)||(n==0))
		{
			perror("�������� �����������");
			continue;
		}
		else
			break;
	}
	sp=n-2;
	if(n==1)
	{
		for(i=0;i<len;i++)
		{
			if(str[i]!=' ')
				++col;
			else
				break;
		}
		for(i=0;i<len;i++)
		{
			str[i]=str[i+col];
		}
		len-=col;
		col=0;
	}
	else
	{
		for(i=0;i<len;i++) //����� ������� �����
		{
			if(str[i]!=' ')
				continue;
			else if((str[i]==' ')&&(count<sp))
			{
				++count;
			}
			else if((str[i]==' ')&&(count==sp))
			{
				k=++i;
				break;
			}	
			else
				break;	
		}
		for(i=k;i<k+20;i++) //������� �������� � �����
		{
			if(str[i]!=' ')
			{
				col++;
			}
			else
			{
				k=i-col;
				break;
			}
		}
		for(i=k;i<len;i++)
		{
			str[i]=str[i+col];
		} 
		len-=col; 
	} 
	for(i=0;i<len;i++)
		printf("%c", str[i]);
	printf("\n");
	system("pause");
	return 0;
}