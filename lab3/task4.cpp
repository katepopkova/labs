/*
Написать программу, которая находит сумму чисел во введённой строке 
Замечание: 
Программа рассматривает непрерывные последовательности цифр в строке как 
числа и обрабатывает их как единое целое. В программе предусмотреть ограни- 
чение на максимальное число разрядов, то есть если пользователь вводит очень 
длинную последовательность цифр, её нужно разбить на несколько групп. 
*/ 
#include<stdio.h> 
#include<locale.h> 
#include<math.h> 
#include<ctype.h> 
#include<string.h> 
int main() 
{ 
	char str[100]; 
	int i, k, len, count=0, sum=0, n, cel, ost, l, p, index; 
	setlocale(LC_ALL, "rus"); 
	printf("Введите строку: "); 
	fgets(str,100,stdin); 
	printf("Введите количество разрядов: "); 
	scanf("%d", &n); 
	len=strlen(str); 
	for(i=0; i<len; i++)
	{ 
		if(isdigit(str[i])!=0) //если цифра 
		{  
			++count; 
		} 
		else if((isdigit(str[i])==0)&&(isdigit(str[i-1])!=0)) // если не цифра 
		{ 
			index=(count-i)*(-1);
			if(count==n) //количество символов не превышает разрядность
			{ 
				p=n-1;	 
				for(k=0;k<n;k++) 
				{ 
					sum+=((int)str[index]-48)*(int)pow(10.0,p); 
					--p; 
					++index;
				} 
			} 
			else if(count>n) //количество символов превышает разрядность
			{ 
				cel=count/n; 
				ost=count%n; 
				for(l=0;l<cel;l++) 
				{ 
					p=n-1; 
					for(k=0;k<n;k++) 
					{ 
						sum+=((int)str[index]-48)*(int)pow(10.0,p); 
						--p; 
						++index;
					} 
				} 
				p=0;
				index+=ost-1;
				for(l=0;l<ost;l++)
				{
					sum+=((int)str[index]-48)*(int)pow(10.0,p); 
					++p; 
					--index;
				}
			}
			count=0;
		} 
		else
			continue;
	} 
	printf("%s%d%c\n", "Сумма равна ", sum, '.'); 


	return 0; 
}