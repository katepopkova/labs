/*
Ќаписать программу, котора€ формирует целочисленный массив размера N, а
затем находит сумму элементов, расположенным между первым отрицатель-
ным и последним положительным элементами.
«амечание:
ћассив заполн€етс€ случайными числами: отрицательными и положительны-
ми поровну (или почти поровну...)
*/
#include<stdio.h>
#include<locale.h>
#include<time.h>
#include<stdlib.h>
int main()
{
	int arr[200], N, i, neg=0, pos=0, diff, sum=0, ind1, ind2;
	setlocale(LC_ALL, "rus");
	printf("¬ведите размер массива: ");
	scanf("%d", &N);
	srand(time(NULL));
	for(i=0;i<N;i++)
	{
		arr[i]=400-rand()%800; //числа от -399 до 400
		if(arr[i]<0)
			++neg;
		else if(arr[i]>0)
			++pos;
		else
			continue;
		diff=neg-pos;
		if((diff==0)&&(abs(diff)==1))
			continue;
		else if(diff>1)
			arr[i]=arr[i]*(-1); //если больше отрицательных, то меняем их на положительные
		else
			arr[i]=arr[i]*(-1); //и наоборот
	}
	for(i=0;i<N;i++) //поиск индекса первого отрицательного элемента
	{
		if(arr[i]<0)
		{
			ind1=i;
			break;
		}
		else
			continue;
	}
	for(i=N;i>0;i--) //поиск индекса последнего положительного элемента
		if(arr[i]>0)
		{
			ind2=i;
			break;
		}
		else
			continue;
	for(i=ind1+1;i<ind2;i++)
		sum+=arr[i];
	printf("%s%d%s\n", "Сумма равна ", sum, '.');
	return 0;
}