/*
�������� ���������, ������� ��������� � ��������� ����������
������� ����������� ����������� � ������� ��� �� ������� (��.
�������).
*/

#include <stdio.h>
#include <stdlib.h>

#define SIZE 30
#define A 4
#define B 6
#define SIZE_FR 9

void clearFrac(char (*fr)[SIZE_FR]);
void clearArr(char (*arr)[SIZE]);
void makeFrac(char (*fr)[SIZE_FR]);
void MakePicVert(char (*arr)[SIZE], char (*fr)[SIZE_FR], int x, int y, int x_cent, int y_cent);
void MakePicHor(char (*arr)[SIZE], char (*fr)[SIZE_FR], int x, int y, int x_cent, int y_cent);
void printArr(char (*arr)[SIZE]);

int main()
{
	char arr[SIZE][SIZE];
	char fr[SIZE_FR][SIZE_FR];
	int x, y, x_cent, y_cent;
	x_cent=13;
	y_cent=4;
	x=9;
	y=0;
	clearFrac(fr);
	clearArr(arr);
	makeFrac(fr);
	MakePicVert(arr, fr, x, y, x_cent, y_cent);
	x_cent=4;
	y_cent=13;
	x=0;
	y=9;
	MakePicHor(arr, fr, x, y, x_cent, y_cent);
	printArr(arr);
	printf("\n");
	system("pause");
	return 0;
}

void clearFrac(char (*fr)[SIZE_FR]) //������� ������� ������� ��������
{
	int i, j;
	for(i=0; i<SIZE_FR; i++)
		for(j=0; j<SIZE_FR; j++)
			fr[i][j]=' ';
}

void clearArr(char (*arr)[SIZE]) //������� ������� ������� ����������
{
	int i, j;
	for(i=0; i<SIZE; i++)
		for(j=0; j<SIZE; j++)
			arr[i][j]=' ';
}

void makeFrac(char (*fr)[SIZE_FR]) //������� ��� �������� ��������
{
	int i, j;
	for(i=0; i<A; i++) //�������� ������� ����� ��������
		fr[i][4]='*';
	for(i=3; i<B; i++)
		fr[1][i]='*';
	for(i=0; i<(SIZE_FR-1)/2; i++) //�������� ������ ����� 
		for(j=0; j<SIZE_FR; j++)
			fr[(SIZE_FR-1)-i][j]=fr[i][j];
	for(i=0; i<A; i++) //�������� ����� �����
		fr[4][i]='*';
	for(i=3; i<B; i++)
		fr[i][1]='*';
	for(i=SIZE_FR-1; i>A; i--) //�������� ������ �����
		fr[4][i]='*';
	for(i=3; i<B; i++)
		fr[i][7]='*';
	fr[4][4]='*';	
}

void MakePicVert(char (*arr)[SIZE], char (*fr)[SIZE_FR], int x, int y, int x_cent, int y_cent) 
{
	int i, j, k, l;
	if(arr[x_cent][y_cent]==' ')
	{
		for(i=x, k=0; i<SIZE_FR+x, k<SIZE_FR; i++, k++)
			for(j=y, l=0; j<SIZE_FR+y, l<SIZE_FR; j++, l++)
				arr[i][j]=fr[k][l];
		MakePicVert(arr, fr, x, y+9, x_cent, y_cent+9);
	}
}

void MakePicHor(char (*arr)[SIZE], char (*fr)[SIZE_FR], int x, int y, int x_cent, int y_cent)
{
	int i, j, k, l;
	if(arr[x_cent][y_cent]==' ')
	{
		for(i=x, k=0; i<SIZE_FR+x, k<SIZE_FR; i++, k++)
			for(j=y, l=0; j<SIZE_FR+y, l<SIZE_FR; j++, l++)
				arr[i][j]=fr[k][l];
		MakePicHor(arr, fr, x+18, y, x_cent+18, y_cent);
	}
}

void printArr(char (*arr)[SIZE]) //������ ������� 
 {
	int i, j;
	for(i=0; i<SIZE; i++)
	{
		for(j=0; j<SIZE; j++)
			printf("%c", arr[i][j]);
		printf("\n");
	}
}


