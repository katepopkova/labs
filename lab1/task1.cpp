#include <locale.h>
#include <stdio.h>
#include <math.h>
int p;
float r, v, rez;
int main ()
{
	setlocale(LC_ALL, "rus");
	printf("Введите пол (0-мужской, 1-женский): ");
	scanf("%d", &p);
	printf("Введите рост(м): ");
	scanf("%f", &r);
	printf("Введите вес(кг): ");
	scanf("%f", &v);
	rez=v/pow(r, 2);
	if(rez<18.5)
		printf("У Вас недостаточный вес! \n");
	if((rez>18.5)&&(rez<25.0))
		printf("Ваш вес в норме! \n");
	if(rez>25.0)
		printf("Избыток веса! Вам следует похудеть! \n");
	return 0;
}