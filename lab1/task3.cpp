#include <locale.h>
#include <stdio.h>
float val, rez;
char ch;
int main()
{
	setlocale(LC_ALL, "rus");
	printf("Введите значение (радианы/градусы): ");
	scanf("%f%c", &val, &ch);
	if(ch=='D')
	{
		rez=val*3.14/180;
		ch='R';
	}
	else
	{
		rez=val*180/3.14;
		ch='D';
	}
	printf("%f%c\n", rez, ch);
	return 0;
}