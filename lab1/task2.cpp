#include <stdio.h>
#include <locale.h>
int hh;
int main()
{
	setlocale (LC_ALL, "rus");
	printf("Введите текущее время в формате ЧЧ:ММ:СС: ");
	scanf("%d", &hh);
	if((hh>=0)&&(hh<5))
		printf("Доброй ночи!\n");
	if((hh>=5)&&(hh<12))
		printf("Доброе утро!\n");
	if((hh>=12)&&(hh<18))
		printf("Добрый день!\n");
	if((hh>=18)&&(hh<=23))
		printf("Добрый вечер!\n");
	return 0;
}