/*
�������� ���������, �������������� ������� � ������� �������� �������:
������� ���� ��������� �����, ����� �����. ������ �������� � ���� �������-
�� � ���� ��������� ������������������ ���� � ����. ������������ �������-
�������� ��������� ������.
���������:
���������� � ������ ������ ����������� ����� �� ������������. ����� ��-
���������� ����������� ��������� �������.
*/

#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define N 150
int main()
{
	char str[N];
	int i, j;
	int len, letCount=0, digCount=0, count=0, s;
	setlocale(LC_ALL, "rus");
	printf("������� ������: ");
	gets(str);
	len=strlen(str);
	for(i=0,j=N-1; i<len; i++,j--) //������� ����� � ����� �������
	{
		if(isdigit(str[i])==0)
		{
			++letCount;
			++j;
		}
		else
		{
			++digCount;
			str[j]=str[i];
		}
	}
	for(i=0; i<len; i++) //����������� ���� � ������ �������
	{
		if(isdigit(str[i]!=0))
		{
			++count;
			continue;
		}
		str[i-count]=str[i];
	}
	s=N-1;
	while(digCount>0) //"������" �������
	{
		str[letCount]=str[s];
		--s;
		++letCount;
		--digCount;
	}
	for(i=0; i<len; i++) //����� �������
		printf("%c", str[i]);
	printf("\n"); 
	system("pause");
	return 0;
}