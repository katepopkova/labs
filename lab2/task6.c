/*
Написать программу, очищающую строку от лишних пробелов. Лишними счи-
таются пробелы в начале строки, в конце строки и пробелы между словами,
если их количество больше 1.
Замечание:
В данной программе запрещёно создавать дополнительные массивы, то есть
необходимо стремиться к экономии памяти. Время выполнения программы зна-
чения не имеет.
*/

#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 200
int main()
{
	char str[N];
	int len, count=0;
	int i, j;
	setlocale(LC_ALL, "rus");
	printf("Введите строку: \n");
	gets(str);
	len=strlen(str);
	while(1) 
	{
		if(str[count]==' ')
		{
			++count;
		}
		else
			break;
	}
	len-=count; //новая длина строки
	for(i=0; i<len; i++) //удаление начальных пробелов
	{
		str[i]=str[i+count];
	}
	count=0;
	j=len;
	while(j>0) //конечные пробелы
	{
		if(str[j]==' ')
		{
			++count;
			--j;
		}
		else
			break;
	}
	len-=count;
	count=0;
	for(i=0; i<len; i++) //удаление пробелов между словами
	{
		if(str[i]==' ' && str[i+1]==' ')
		{
			++count;
			continue;
		}
		str[i-count]=str[i];
	} 
	len-=count;
	
	for(i=0;i<len;i++)
	{
		printf("%c", str[i]);
	}
	printf("\n");
	system("pause");
	return 0;
}